# -*- coding: utf-8 -*-
import argparse
import configparser
import logging
import os

import re
from hashlib import md5

import xlrd

from string import Formatter


class ExtendedFormatter(Formatter):
    def __init__(self, *args, **kwargs):
        self._key_translator = kwargs.pop('key_translator') if 'key_translator' in kwargs else None
        super(ExtendedFormatter, self).__init__(*args, **kwargs)

    def set_key_translator(self, translator):
        if callable(translator):
            self._key_translator = translator

    def format_field(self, value, format_spec):
        if format_spec.endswith('i'):
            try:
                value = str(int(float(value)))
            except:
                value = ''
            return value
        elif format_spec.endswith('s'):
            value = str(value)
        elif re.match(r'(:?\d+#)?md5', format_spec):
            value = str(value)
            size = int(format_spec.split('#')[0]) if '#' in format_spec else None
            h = md5()
            if os.path.isfile(value):
                with open(value, 'rb') as f:
                    chunk_size = 1024
                    for chunk in iter(lambda: f.read(chunk_size), ''):
                        if size is not None:
                            if size <= 0:
                                break
                            chunk = chunk[:size]
                            size -= chunk_size
                        h.update(chunk)
            else:
                h.update(value.encode())
            return h.hexdigest()
        return super(ExtendedFormatter, self).format_field(value, format_spec)

    def get_value(self, key, args, kwargs):
        if self._key_translator:
            key = self._key_translator(key)
        return super(ExtendedFormatter, self).get_value(key, args, kwargs)


def coroutine(func):
    def wrapper(*args, **kwargs):
        cr = func(*args, **kwargs)
        next(cr)
        return cr

    return wrapper


@coroutine
def writer(filename):
    with open(filename, 'w', encoding='utf-8') as f:
        while True:
            data = (yield)
            if data:
                f.write(data + '\n')


def index_by_letter(name):
    index = None
    if isinstance(name, int):
        index = name
    elif isinstance(name, str):
        if re.match(r'[a-z]+', name, re.IGNORECASE):
            index = 0
            grade = 1
            for c in name[::-1]:
                index += (ord(c.upper()) - 64) * grade
                grade *= 26
        elif name.isdigit():
            index = int(name)
    return index - 1


@coroutine
def template_resolver(template=None, target=None):
    template = template if isinstance(template, str) else ''
    formatter = ExtendedFormatter(key_translator=index_by_letter)
    while target and template:
        row_data = (yield)
        if row_data:
            target.send(formatter.format(template, *row_data) + '\n')


def parse_excel(filename, sheet_index=0, start_row=1, targets=None):
    targets = targets if isinstance(targets, (list, tuple, set)) else []
    if not targets:
        logging.warning('Пустой набор шаблонов')
        return

    logging.info('Открытие файла/книги {}'.format(os.path.abspath(filename)))
    book = xlrd.open_workbook(filename)
    logging.info('Открытие листа {}'.format(sheet_index))
    sheet = book.sheet_by_index(sheet_index)

    logging.info('Обработка данных'.format(sheet.nrows))
    verbose_interval = sheet.nrows // 100 or 1
    formatter = ExtendedFormatter()
    formatter.set_key_translator(index_by_letter)
    for row in range(start_row, sheet.nrows):
        if (row % verbose_interval == 0) or (row == sheet.nrows - 1):
            print(row + 1, end='\r', flush=True)
        row_data = sheet.row_values(row)
        if row_data:
            for condition, target in targets:
                if not condition or eval(formatter.format(condition, *row_data)):
                    target.send(row_data)
    print(row, flush=True)


def get_rules(settings):
    rules = []
    for section in settings.sections():
        condition = settings.get(section, 'condition', fallback=None)
        template = settings.get(section, 'template', fallback=None)
        template_file = settings.get(section, 'template_file', fallback=None)
        if template_file and os.path.isfile(template_file):
            with open(template_file, 'r', encoding='utf-8') as t:
                template = t.read()
        if template:
            rules.append((template, condition))
    return rules


def main():
    parser = argparse.ArgumentParser(description='Создает файл, в который для каждой строки исходного excel-файла '
                                                 'вставляет содержимое всех шаблонов, чьим условиям соответствуют '
                                                 'данные строки, подставляя в шаблон данные этой строки')
    parser.add_argument('--debug', dest='debug', action='store_true', default=False, help='Вывод отладочной информации')
    parser.add_argument('-o', '--out-file', required=True, dest='out_file', help='Итоговый файл')
    parser.add_argument('-s', '--settings', required=True, dest='settings', default=None, help='Файл с настройками')
    parser.add_argument('source', default=None, help='исходный excel-файл с данными о пациенте')

    options = parser.parse_args()
    logging.getLogger().setLevel(logging.DEBUG if options.debug else logging.INFO)

    settings = configparser.ConfigParser()
    if options.settings:
        settings.read(options.settings, encoding='utf-8')

    sheet = settings.getint('general', 'sheet', fallback=0)
    from_row = settings.getint('general', 'from_row', fallback=1)

    out_writer = writer(options.out_file)

    parse_excel(options.source,
                sheet,
                from_row,
                [(condition, template_resolver(template, out_writer))
                 for template, condition in get_rules(settings)])


if __name__ == '__main__':
    main()
