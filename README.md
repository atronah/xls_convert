Excel file converter
====================

Python sripts for converting .xls and  .xlsx files into specific forms, i.e. sql-queries.


Scripts
=======

convert.py
----------
Reads data line by line from Excel-file and pass values of its cells into template string (by substituting format string),
specified in config file, and write result string into output file.

Config file has the following format:
```ini
[general]
; Number of sheet in source Excel-fule to read data
sheet = 0
; Number of first data row (counting start at zero)
from_row = 1


; Arbitrary rule. If more than one rule, than all of them processed for each source data line with checking condition
[rule1]
; template string to write into output file after substituting values from values of source line
template =
; substituted by values from source line python boolean expression,
; to check whether it need to use a template from rule for given line
condition =
```


make_fr3.py
-----------
Converts Excel file to FastReport report file with the same design. Creates FastReport pages for each sheet in source Excel file.