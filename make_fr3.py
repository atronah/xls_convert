# -*- coding: utf-8 -*-
import os
from datetime import datetime

import xlrd
import argparse
import xml.etree.ElementTree as etree


def resize(size):
    fr3_scale = 37.7953
    return fr3_scale * round(size, 1)


def format_float(value):
    return str(value).replace('.', ',')


class XF:
    HOR_ALIGN_GENERAL = 0
    HOR_ALIGN_LEFT = 1
    HOR_ALIGN_CENTRED = 2
    HOR_ALIGN_RIGHT = 3
    HOR_ALIGN_FILLED = 4
    HOR_ALIGN_JUSTIFIED = 5
    HOR_ALIGN_CENTRED_ACROSS_SELECTION = 6
    HOR_ALIGN_DISTRIBUTED = 7

    VERT_ALIGN_TOP = 0
    VERT_ALIGN_CENTRED = 1
    VERT_ALIGN_BOTTOM = 2
    VERT_ALIGN_JUSTIFIED = 3
    VERT_ALIGN_DISTRIBUTED = 4

    hor_align_map = {HOR_ALIGN_CENTRED: 'haCenter',
                     HOR_ALIGN_RIGHT: 'haRight',
                     HOR_ALIGN_FILLED: 'haBlock'}

    vert_align_map = {VERT_ALIGN_CENTRED: 'vaCenter',
                      VERT_ALIGN_BOTTOM: 'vaBottom'}

    BORDER_DOT = 3
    BORDER_DASH = 4
    border_map = {BORDER_DOT: 'fsDot',
                  BORDER_DASH: 'fsDash'}

    font_height_map = {5: '-7',
                       6: '-8',
                       7: '-9',
                       8: '-11',
                       9: '-12',
                       10: '-13',
                       11: '-15',
                       12: '-16',
                       14: '-19',
                       16: '-21',
                       18: '-24',
                       20: '-27',
                       22: '-29',
                       24: '-32',
                       26: '-35',
                       28: '-37',
                       36: '-48',
                       48: '-64',
                       72: '-96'
                       }

    def __init__(self, book, xf_index):
        self._book = book
        self._xf_index = xf_index

    def memo_attr(self):
        attr = {}
        xf = self._book.xf_list[self._xf_index]
        if xf:
            if xf.alignment.hor_align in self.hor_align_map.keys():
                attr['HAlign'] = self.hor_align_map[xf.alignment.hor_align]
            if xf.alignment.vert_align in self.vert_align_map.keys():
                attr['VAlign'] = self.vert_align_map[xf.alignment.vert_align]
            font = self._book.font_list[xf.font_index]
            attr['Font.Style'] = str(int(font.bold) |
                                     int(font.italic) << 1 |
                                     int(font.underlined) << 2)
            attr['Font.Name'] = font.name
            attr['Font.Height'] = self.font_height_map.get(font.height / 20, '-13')
            if (xf.border.left_line_style or
                    xf.border.right_line_style or
                    xf.border.top_line_style or
                    xf.border.bottom_line_style):
                attr['Frame.Typ'] = str(int(bool(xf.border.left_line_style)) |
                                        int(bool(xf.border.right_line_style)) << 2 |
                                        int(bool(xf.border.top_line_style)) << 4 |
                                        int(bool(xf.border.bottom_line_style)) << 8)
                if xf.border.left_line_style in self.border_map.keys():
                    attr['Frame.Style'] = self.border_map[xf.border.left_line_style]
                elif xf.border.right_line_style in self.border_map.keys():
                    attr['Frame.Style'] = self.border_map[xf.border.right_line_style]
                elif xf.border.top_line_style in self.border_map.keys():
                    attr['Frame.Style'] = self.border_map[xf.border.top_line_style]
                elif xf.border.bottom_line_style in self.border_map.keys():
                    attr['Frame.Style'] = self.border_map[xf.border.bottom_line_style]

        return attr


def parse_xls(source):
    book = xlrd.open_workbook(source, formatting_info=True)

    code_section = '''
    begin
        db.Connected := False;
        db.Handle := <DBHandle>;
    end.
    '''

    data_types = (xlrd.XL_CELL_BOOLEAN, xlrd.XL_CELL_DATE, xlrd.XL_CELL_NUMBER, xlrd.XL_CELL_TEXT)

    report = etree.Element('TfrxReport',
                           {'Version': '4.12',
                            'DotMatrixReport': 'False',
                            'IniFile': '\Software\Fast Reports',
                            'ScriptLanguage': 'PascalScript',
                            'ScriptText.Text': code_section,
                            # 'PropData': ''
                            })
    xml = etree.ElementTree(report)
    data_page = etree.SubElement(report,
                                 'TfrxDataPage',
                                 {'Name': 'Data',
                                  'Height': '1000',
                                  'Left': '0',
                                  'Top': '0',
                                  'Width': '1000',
                                  })
    WIDTH_BASE_DIVIDER = 256
    WIDTH_DECORATE_DIVIDER = 5.25
    HEIGHT_BASE_DIVIDER = 20
    HEIGHT_DECORATE_DIVIDER = 30
    DATE_MODE = 0

    for sheet_idx, sheet in enumerate(book.sheets()):
        page = etree.SubElement(report,
                                'TfrxReportPage',
                                {'Name': 'Page{}'.format(sheet_idx + 1),
                                 'PaperWidth': '210',
                                 'PaperHeight': '297',
                                 'PaperSize': '9',
                                 'LeftMargin': '10',
                                 'RightMargin': '10',
                                 'TopMargin': '10',
                                 'BottomMargin': '10',
                                 'ColumnWidth': '0',
                                 })

        column_offsets = [0]
        row_offsets = [0]
        memos = {}
        for row in range(sheet.nrows):
            row_offsets.append(row_offsets[-1] + (sheet.rowinfo_map[row].height if row in sheet.rowinfo_map
                                                  else sheet.default_row_height))
            for column, cell in enumerate(sheet.row(row)):
                if column + 1 >= len(column_offsets):
                    column_offsets.append(column_offsets[-1] + sheet.computed_column_width(column))
                if cell.ctype in data_types:
                    value = cell.value
                    if cell.ctype == xlrd.XL_CELL_DATE:
                        value = datetime(*xlrd.xldate_as_tuple(value, DATE_MODE))
                    memos[row, column] = {'name': 'Memo{}_{}_{}'.format(sheet_idx + 1, row + 1, column + 1),
                                          'start_row': row, 'end_row': row,
                                          'start_column': column, 'end_column': column,
                                          'text': value,
                                          'xf_index': cell.xf_index}

        for start_row, end_row, start_column, end_column in sheet.merged_cells:
            if (start_row, start_column) not in memos:
                continue
            memo = memos[start_row, start_column]
            memo['end_row'] = end_row - 1
            memo['end_column'] = end_column - 1

        column_offsets = [resize(val / WIDTH_BASE_DIVIDER / WIDTH_DECORATE_DIVIDER)
                          for val in column_offsets]
        row_offsets = [resize(val / HEIGHT_BASE_DIVIDER / HEIGHT_DECORATE_DIVIDER)
                       for val in row_offsets]

        page_header = etree.SubElement(page,
                                       'TfrxPageHeader',
                                       {'Name': 'PageHeader{}'.format(sheet_idx + 1),
                                        'Height': format_float(row_offsets[-1]),
                                        })
        for memo in memos.values():
            if not memo.get('text', None):
                continue
            left = column_offsets[memo['start_column']]
            top = row_offsets[memo['start_row']]
            right = column_offsets[memo['end_column'] + 1]
            bottom = row_offsets[memo['end_row'] + 1]
            add_memo(page_header,
                     memo['name'],
                     (format_float(left), format_float(top), format_float(right - left), format_float(bottom - top)),
                     memo['text'],
                     XF(book, memo['xf_index'])
                     )
    xml.write(os.path.splitext(source)[0] + '.fr3', encoding='utf-8', xml_declaration=True)


def add_memo(parent, name, geometry, text, xf):
    left, top, width, height = geometry
    attr = {'Name': name,
            'Left': left,
            'Top': top,
            'Width': width,
            'Height': height,
            'ParentFont': 'False',
            'Text': str(text)
            }
    attr.update(xf.memo_attr())
    memo = etree.SubElement(parent,
                            'TfrxMemoView',
                            attr)
    return memo


def main():
    opt_parser = argparse.ArgumentParser()
    opt_parser.add_argument('source')
    options = opt_parser.parse_args()

    parse_xls(options.source)

if __name__ == '__main__':
    main()
