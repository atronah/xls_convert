# -*- coding: utf-8 -*-
import argparse
import configparser
import logging
import os

import xlrd
import xlwt
import yaml
import openpyxl


def main():
    parser = argparse.ArgumentParser(description='Подготавливает Excel-файл к загрузке в МИС Инфоклиника'
                                                 ' согласно указанным в ini-файле настройкам')
    parser.add_argument('--debug', dest='debug', action='store_true', default=False, help='Вывод отладочной информации')
    parser.add_argument('-s', '--settings', required=True, dest='settings', default='conf.yaml', help='Файл с настройками')
    parser.add_argument('source', default=None, help='Исходный Excel-файл')

    options = parser.parse_args()

    logging.getLogger().setLevel(logging.DEBUG if options.debug else logging.INFO)

    settings = {}
    with open(options.settings, 'rt', encoding='utf-8') as s:
        settings = yaml.safe_load(s)

    logging.info(f'Открытие файла/книги "{os.path.abspath(options.source)}"')

    is_xls = os.path.splitext(options.source)[1] == '.xls'

    book = xlrd.open_workbook(options.source) if is_xls else openpyxl.load_workbook(options.source)

    sheets_settings = settings.get('sheets', dict())

    for sheet_name, sheet_info in sheets_settings.items():
        logging.info(f'Открытие листа "{sheet_name}"')
        if isinstance(sheet_name, int) \
                or (isinstance(sheet_name, str) and sheet_name.isdigit() and sheet_name not in book.sheet_names()):
            sheet = book.sheet_by_index(int(sheet_name)) if is_xls else book[book.sheetnames[int(sheet_name)]]
            logging.info(f'Имя листа: {sheet_name}')
        else:
            sheet = book.sheet_by_name(str(sheet_name)) if is_xls else book[sheet_name]

        new_sheet_name = sheet_info.get('new_name', str(sheet_name))
        header_row = sheet_info.get('header_row', None)
        header_row_data = None
        data_first_row = sheet_info.get('data_first_row', 0)
        columns_settings = sheet_info.get('columns', dict())

        new_book = openpyxl.Workbook()
        new_sheet = new_book.active
        new_sheet.title = new_sheet_name
        logging.debug(f'new_sheet_name: {new_sheet_name}')
        logging.debug(f'header_row: {header_row}')
        logging.debug(f'data_first_row: {data_first_row}')
        logging.debug(f'columns_settings.keys(): {columns_settings.keys()}')
        new_row = 0
        for row, row_data in enumerate(sheet.get_rows() if is_xls else sheet.rows):
            if header_row_data is None:
                if header_row is None:
                    logging.debug(f'using columns, described in yaml as header info')
                    header_row_data = list(columns_settings.keys())
                elif row == header_row:
                    header_row_data = row_data
                    logging.debug(f'using row {row} as header info: {",".join([c.value if isinstance(c, (xlrd.sheet.Cell, openpyxl.worksheet.worksheet.Cell)) else c for c in header_row_data])}')

                if header_row_data:
                    new_column_idx = 0
                    for column_idx, header_cell_data in enumerate(header_row_data):
                        if isinstance(header_cell_data, (xlrd.sheet.Cell, openpyxl.worksheet.worksheet.Cell)):
                            column_name = header_cell_data.value
                            logging.debug(f'processing column [{column_idx}] "{column_name}" from header_cell_data.value')
                        elif isinstance(header_cell_data, str):
                            column_name = header_cell_data
                            logging.debug(f'processing column [{column_idx}] "{column_name}" from header_cell_data')
                        elif isinstance(header_cell_data, int):
                            column_idx = header_cell_data
                            column_name = sheet.cell(row + 1, column_idx + 1).value if header_row is not None else None
                            logging.debug(f'processing column [{column_idx}] "{column_name}" from sheet.cell({row + 1}, {column_idx + 1})')
                        else:
                            column_name = None
                            logging.debug(f'empty column name for column with index {column_idx}')

                        if column_idx in columns_settings:
                            column_info = columns_settings[column_idx]
                        else:
                            column_info = columns_settings.get(column_name, {})
                            columns_settings[column_idx] = column_info

                        if column_info:
                            column_info['new_idx'] = new_column_idx
                            new_name = column_info.get('new_name', column_name)
                            new_sheet.cell(new_row + 1, new_column_idx + 1, new_name)
                            logging.debug(f'... new index for [{column_idx}] "{column_name}" is {new_column_idx}')
                            new_column_idx += 1
                        else:
                            logging.debug(f'... skipped column [{column_idx}] "{column_name}"')
            if row >= data_first_row:
                new_row += 1
                num_format_str = columns_settings.get(column_idx, dict()).get('num_format_str', None)
                for column_idx, column_data in enumerate(row_data):
                    new_column_idx = columns_settings.get(column_idx, {}).get('new_idx', None)
                    if new_column_idx is None:
                        continue
                    if is_xls and column_data.ctype == xlrd.XL_CELL_DATE:
                        new_value = xlrd.xldate.xldate_as_datetime(column_data.value, book.datemode)
                        if num_format_str is None:
                            num_format_str = 'dd.mm.yyyy'
                        # style = xlwt.Style.easyxf(num_format_str=num_format_str)
                    else:
                        new_value = column_data.value
                        # style = xlwt.Style.easyxf()
                    new_sheet.cell(new_row + 1, new_column_idx + 1, new_value)
                if row < 10 or row % 1000 == 0:
                    logging.debug(f'processed data from from source row {row} to target row {new_row}')

        new_book_filename = str(sheet_name) + '.xlsx'
        logging.info(f'Сохранение файла/книги "{new_book_filename}"')
        new_book.save(new_book_filename)


if __name__ == '__main__':
    main()
